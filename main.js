const { DateTime, Interval } = require('luxon');

const isValidName = (name) => {
    if (!isInputCorrect(name)) return false;
    const validCharacters = !!name.match(/^[A-Za-zÀ-ÖØ-öø-ÿ'\-. ]{2,20}$/);
    if (!validCharacters) return false;
    const tooManySymbols = !!name.match(/\W{3,20}/);
    if (tooManySymbols) return false;
    return true;
}

const isValidEmail = (email) => {
    if (!isInputCorrect(email)) return false;
    // From https://emailregex.com/
    return !!email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
}

const isValidBirthDate = (date, format='dd/MM/yyyy') => {
    if (!isInputCorrect(date)) return false;
    const dateObj = DateTime.fromFormat(date, format);
    if (dateObj.invalid) return false;
    const isRealBirth = Interval.fromDateTimes(
        DateTime.local().minus({ year: 110 }),
        DateTime.local().minus({ day: 1 })
    ).contains(dateObj);
    if (!isRealBirth) return false;
    return true;
}

const isValidZipCode = (zipcode) => {
    if (!isInputCorrect(zipcode)) return false;
    return !!zipcode.match(/^[0-9]{5}$/);
}

const isInputCorrect = (input) => {
    return (
            !input ||
            typeof input != 'string' ||
            !input.trim()
            ) ? false : true;
}

module.exports = {
    isValidName,
    isValidSurname: isValidName,
    isValidEmail,
    isValidBirthDate,
    isValidZipCode
}
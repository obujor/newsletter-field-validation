const validator = require('../');
const assert = require('assert');

describe('validateName', () => {
  describe('empty/not defined input', () => {
    it('should return false when passed empty string', () => {
      assert.equal(validator.isValidName(''), false);
    });
    it('should return false when passed undefined', () => {
      assert.equal(validator.isValidName(undefined), false);
    });
    it('should return false when passed null', () => {
      assert.equal(validator.isValidName(null), false);
    });
    it('should return false when passed false', () => {
      assert.equal(validator.isValidName(false), false);
    });
  });

  describe('not valid names', () => {
    it('should return false when passed strings with only spaces', () => {
      assert.equal(validator.isValidName(' '), false);
      assert.equal(validator.isValidName('      '), false);
    });
    it('should return false when passed too short string', () => {
      assert.equal(validator.isValidName('b'), false);
    });
    it('should return false when passed a string containing not allowed characters', () => {
      assert.equal(validator.isValidName('Mario++'), false);
      assert.equal(validator.isValidName('+++'), false);
      assert.equal(validator.isValidName('Foo=='), false);
      assert.equal(validator.isValidName('Foo123'), false);
      assert.equal(validator.isValidName('12345'), false);
      assert.equal(validator.isValidName('(ab)'), false);
    });
    it('should return false when passed a string containing only allowed symbols', () => {
      assert.equal(validator.isValidName('\'\'\'\''), false);
      assert.equal(validator.isValidName('  --- -.'), false);
      assert.equal(validator.isValidName('...'), false);
    });
    it('should return false when passed a string containing too many allowed symbols', () => {
      assert.equal(validator.isValidName('Mario\'\'\'\''), false);
      assert.equal(validator.isValidName('Pie---tro'), false);
    });
  });

  describe('valid names', () => {
    it('should return true when passed simple names', () => {
      assert.equal(validator.isValidName('Mario'), true);
      assert.equal(validator.isValidName('Federico'), true);
      assert.equal(validator.isValidName('Jack'), true);
    });
    it('should return true when passed names with symbols', () => {
      assert.equal(validator.isValidName('De Mario'), true);
      assert.equal(validator.isValidName('D\'angelo'), true);
      assert.equal(validator.isValidName('Yoo-Lee'), true);
      assert.equal(validator.isValidName('P.Morgan'), true);
    });
  });
});

describe('validSurname', () => {
  it('should be equal to validName', () => {
    assert.strictEqual(validator.isValidSurname, validator.isValidName);
  });
});

describe('validateEmail', () => {
  describe('empty/not defined input', () => {
    it('should return false when passed empty string', () => {
      assert.equal(validator.isValidEmail(''), false);
    });
    it('should return false when passed undefined', () => {
      assert.equal(validator.isValidEmail(undefined), false);
    });
    it('should return false when passed null', () => {
      assert.equal(validator.isValidEmail(null), false);
    });
    it('should return false when passed false', () => {
      assert.equal(validator.isValidEmail(false), false);
    });
  });

  describe('not valid names', () => {
    it('should return false when passed strings with two @', () => {
      assert.equal(validator.isValidEmail('mario@@libero.it'), false);
      assert.equal(validator.isValidEmail('@mario@libero.it'), false);
    });
    it('should return false when passed a string not containing domain', () => {
      assert.equal(validator.isValidEmail('luca@libero'), false);
      assert.equal(validator.isValidEmail('monica@'), false);
    });
  });

  describe('valid emails', () => {
    it('should return true when passed valid emails', () => {
      assert.equal(validator.isValidEmail('andrewik@icloud.com'), true);
      assert.equal(validator.isValidEmail('plover@live.com'), true);
      assert.equal(validator.isValidEmail('plover@aol.com'), true);
      assert.equal(validator.isValidEmail('dleconte@att.net'), true);
      assert.equal(validator.isValidEmail('gomor@optonline.net'), true);
      assert.equal(validator.isValidEmail('sravani@optonline.net'), true);
      assert.equal(validator.isValidEmail('m.rossi@gmail.com'), true);
    });
  });
});

describe('isValidBirthDate', () => {
  describe('empty/not defined input', () => {
    it('should return false when passed empty string', () => {
      assert.equal(validator.isValidBirthDate(''), false);
    });
    it('should return false when passed undefined', () => {
      assert.equal(validator.isValidBirthDate(undefined), false);
    });
    it('should return false when passed null', () => {
      assert.equal(validator.isValidBirthDate(null), false);
    });
    it('should return false when passed false', () => {
      assert.equal(validator.isValidBirthDate(false), false);
    });
  });

  describe('not valid dates', () => {
    it('should return false when passed no date strings', () => {
      assert.equal(validator.isValidBirthDate('mario@@libero.it'), false);
      assert.equal(validator.isValidBirthDate('mario'), false);
    });
    it('should return false when passed no valid dates', () => {
      assert.equal(validator.isValidBirthDate('1000'), false);
      assert.equal(validator.isValidBirthDate('1000/10/2000'), false);
      assert.equal(validator.isValidBirthDate('40/10/2000'), false);
      assert.equal(validator.isValidBirthDate('10/13/2000'), false);
    });
    it('should return false when passed valid dates but out of range', () => {
      assert.equal(validator.isValidBirthDate('13/10/2019'), false);
      assert.equal(validator.isValidBirthDate('13/10/1900'), false);
    });
  });

  describe('valid emails', () => {
    it('should return true when passed valid dates', () => {
      assert.equal(validator.isValidBirthDate('10/10/1990'), true);
      assert.equal(validator.isValidBirthDate('24/02/2009'), true);
    });
    it('should return true when passed valid dates custom format', () => {
      assert.equal(validator.isValidBirthDate('10/10/90', 'MM/dd/yy'), true);
      assert.equal(validator.isValidBirthDate('02/24/09', 'MM/dd/yy'), true);
    });
  });
});

describe('validZipCode', () => {
  describe('empty/not defined input', () => {
    it('should return false when passed empty string', () => {
      assert.equal(validator.isValidZipCode(''), false);
    });
    it('should return false when passed undefined', () => {
      assert.equal(validator.isValidZipCode(undefined), false);
    });
    it('should return false when passed null', () => {
      assert.equal(validator.isValidZipCode(null), false);
    });
    it('should return false when passed false', () => {
      assert.equal(validator.isValidZipCode(false), false);
    });
  });

  describe('not valid zipcode', () => {
    it('should return false when passed wrong length input', () => {
      assert.equal(validator.isValidZipCode(000), false);
      assert.equal(validator.isValidZipCode(0005454), false);
      assert.equal(validator.isValidZipCode('01'), false);
      assert.equal(validator.isValidZipCode('564894897897897'), false);
    });
    it('should return false when passed with letters', () => {
      assert.equal(validator.isValidZipCode('a1a21'), false);
      assert.equal(validator.isValidZipCode('aaaaa'), false);
    });
  });

  describe('valid zipcode', () => {
    it('should return true when passed with correct codes', () => {
      assert.equal(validator.isValidZipCode('00120'), true);
      assert.equal(validator.isValidZipCode('11100'), true);
      assert.equal(validator.isValidZipCode('97100'), true);
      assert.equal(validator.isValidZipCode('45100'), true);
    });
  });
});
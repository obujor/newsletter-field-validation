# newsletter-field-validation

Simple module implementing field validation for Tickete application

## API
All functions return a boolean.
* validator.isValidName(String)
* validator.isValidSurname(String) this is identical to isValidName
* validator.isValidEmail(String)
* validator.isValidBirthDate(String [,formatString]) for formatString details look [here](https://moment.github.io/luxon/docs/manual/formatting.html)
* validator.isValidZipCode(String)

## Usage

Example of usage:
```javascript
const validator = require('newsletter-field-validation');
validator.isValidName('Mario');
validator.isValidSurname('Rossi');
validator.isValidEmail('m.rossi@gmail.com');
validator.isValidBirthDate('10/12/1960');
validator.isValidZipCode('40010');
```

## Test

Run test with `npm run test`

